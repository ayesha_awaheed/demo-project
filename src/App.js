import "./css/App.css";
import "antd/dist/antd.css";
import store from "./redux/store";
import RecordForm from "./components/form";
import RecordTable from "./components/table";

import React, { useState } from "react";

import { Tabs } from "antd";
const { TabPane } = Tabs;

function App() {
  const [data, setData] = useState([]);

  const handleSelect = (key) => {
    const result = store.getState();
    const record = result.map((record) => {
      const resid = record.description.residence;
      return {
        key: record.key,
        Email: record.description.email,
        Password: record.description.password,
        Residence: resid.join("-"),
        Phone: record.description.phone,
        Gender: record.description.gender,
        tags: [record.description.gender],
      };
    });
    setData(record);
  };

  return (
    <div className="App">
      <Tabs
        defaultActiveKey="1"
        className="tabsContainer"
        centered
        onChange={handleSelect}
      >
        <TabPane tab="Form" key="1">
          <RecordForm />
        </TabPane>
        <TabPane tab="Record" key="2" forceRender="true">
          <RecordTable data={data} />
        </TabPane>
      </Tabs>
    </div>
  );
}

export default App;
