import * as actions from "./actionTypes";

let lastid = 0;
export default function reducer(state = [], action) {
    if(action.type === actions.ADD_RECORD)
    return [
        ...state,
        {
            id: ++lastid,
            description: action.payload.description,

        }
    ];
    return state;
}