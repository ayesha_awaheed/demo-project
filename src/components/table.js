import React, { useState, useEffect } from "react";
import { Table, Tag } from "antd";

const RecordTable = (props) => {
  const [records, setRecords] = useState(props.data);

  const columns = [
    {
      title: "Email",
      dataIndex: "Email",
      key: "Email",
      render: (text) => <a >{text}</a>,
    },
    {
      title: "Password",
      dataIndex: "Password",
      key: "Password",
    },
    {
      title: "Residence",
      dataIndex: "Residence",
      key: "Residence",
    },
    {
      title: "Phone",
      dataIndex: "Phone",
      key: "Phone",
    },
    {
      title: "Gender",
      key: "tags",
      dataIndex: "tags",
      render: (tags) => (
        <>
          {tags.map((tag) => {
            let color = tag.length > 4 ? "geekblue" : "green";
            return (
              <Tag color={color} key={tag}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
        </>
      ),
    },
  ];
  useEffect(() => {
    setRecords(props.data);
  });

  return <Table columns={columns} dataSource={records} />;
};

export default RecordTable;
