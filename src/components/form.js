import React, { useState } from "react";
import store from "../redux/store";
import { addRecord } from "../redux/actions";

import {
  Form,
  Input,
  Cascader,
  Select,
  Checkbox,
  Button,
} from "antd";
const { Option } = Select;

const residences = [
  {
    value: "zhejiang",
    label: "Zhejiang",
    children: [
      {
        value: "hangzhou",
        label: "Hangzhou",
        children: [
          {
            value: "xihu",
            label: "West Lake",
          },
        ],
      },
    ],
  },
  {
    value: "jiangsu",
    label: "Jiangsu",
    children: [
      {
        value: "nanjing",
        label: "Nanjing",
        children: [
          {
            value: "zhonghuamen",
            label: "Zhong Hua Men",
          },
        ],
      },
    ],
  },
];
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

const RecordForm = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [phone, setPhone] = useState("");
  const [code, setCode] = useState("86");
  const [residence, setResidence] = useState(["Zhejiang","Hangzhou","West Lake"]);
  const [gender, setGender] = useState("");

  const onFinish = () => {

    let record = {
      email: email,
      password: password,
      phone: code+phone,
      residence: residence,
      gender: gender,
    };
    store.dispatch(addRecord(record));
    document.getElementById("form").reset();
  };

  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select
        style={{
          width: 70,
        }}
        onChange={(event) => {
          setCode(event);
        }}
      >
        <Option value="86">+86</Option>
        <Option value="87">+87</Option>
      </Select>
    </Form.Item>
  );

  return (
    <Form
      {...formItemLayout}
      name="register"
      onFinish={onFinish}
      initialValues={{
        residence: ["zhejiang", "hangzhou", "xihu"],
        prefix: "86",
      }}
      id = "form"
      scrollToFirstError
    >
      <Form.Item
        name="email"
        label="E-mail"
        rules={[
          {
            type: "email",
            message: "The input is not valid E-mail!",
          },
          {
            required: true,
            message: "Please input your E-mail!",
          },
        ]}
        style={{ marginRight: "10%" }}
      >
        <Input
          onChange={(event) => {
            setEmail(event.target.value);
          }}
        />
      </Form.Item>

      <Form.Item
        name="password"
        label="Password"
        rules={[
          {
            required: true,
            message: "Please input your password!",
          },
        ]}
        hasFeedback
        style={{ marginRight: "10%" }}
      >
        <Input.Password
          onChange={(event) => {
            setPassword(event.target.value);
          }}
        />
      </Form.Item>
      <Form.Item
        name="residence"
        label="Habitual Residence"
        rules={[
          {
            type: "array",
            required: true,
            message: "Please select your habitual residence!",
          },
        ]}
        style={{ marginRight: "10%" }}
      >
        <Cascader
          options={residences}
          onChange={(value) => {
            setResidence(value);
          }}
        />
      </Form.Item>

      <Form.Item
        name="phone"
        label="Phone Number"
        rules={[
          {
            required: true,
            message: "Please input your phone number!",
          },
        ]}
        style={{ marginRight: "10%" }}
      >
        <Input
          addonBefore={prefixSelector}
          style={{
            width: "100%",
          }}
          onChange={(event) => {
            setPhone(event.target.value);
          }}
        />
      </Form.Item>

      <Form.Item
        name="gender"
        label="Gender"
        rules={[
          {
            required: true,
            message: "Please select gender!",
          },
        ]}
        style={{ marginRight: "10%" }}
      >
        <Select
          placeholder="select your gender"
          onChange={(value) => {
            setGender(value);
          }}
        >
          <Option value="male">Male</Option>
          <Option value="female">Female</Option>
        </Select>
      </Form.Item>

      <Form.Item
        name="agreement"
        valuePropName="checked"
        rules={[
          {
            validator: (_, value) =>
              value
                ? Promise.resolve()
                : Promise.reject(new Error("Should accept this")),
          },
        ]}
        {...tailFormItemLayout}
        style={{ marginRight: "10%" }}
      >
        <Checkbox>
          I have provided the <a>correct information</a>
        </Checkbox>
      </Form.Item>
      <Form.Item {...tailFormItemLayout} className="formBtn">
        <Button type="primary" htmlType="submit">
          Register
        </Button>
      </Form.Item>
    </Form>
  );
};

export default RecordForm;
